# This is the source.sh script. It is executed by BPM in a temporary directory when compiling a source package
# BPM Expects the source code to be extracted into the automatically created 'source' directory which can be accessed using $BPM_SOURCE
# BPM Expects the output files to be present in the automatically created 'output' directory which can be accessed using $BPM_OUTPUT

DOWNLOAD="https://anduin.linuxfromscratch.org/LFS/ncurses-${BPM_PKG_VERSION}.tar.xz"
FILENAME="${DOWNLOAD##*/}"

# The prepare function is executed in the root of the temp directory
# This function is used for downloading files and putting them into the correct location
prepare() {
  wget "$DOWNLOAD"
  tar -xvf "$FILENAME" --strip-components=1 -C "$BPM_SOURCE"
}

# The build function is executed in the source directory
# This function is used to compile the source code
build() {
  ./configure --prefix=/usr           \
            --mandir=/usr/share/man \
            --with-shared           \
            --without-debug         \
            --without-normal        \
            --with-cxx-shared       \
            --enable-pc-files       \
            --enable-widec          \
            --with-pkg-config-libdir=/usr/lib/pkgconfig
  make
}

# The check function is executed in the source directory
# This function is used to run tests to verify the package has been compiled correctly
check() {
  make check
}

# The package function is executed in the source directory
# This function is used to move the compiled files into the output directory
package() {
  make DESTDIR="$BPM_OUTPUT" install
  sed -e 's/^#if.*XOPEN.*$/#if 1/'  -i "$BPM_OUTPUT"/usr/include/curses.h
  for lib in ncurses form panel menu ; do
    ln -sfv lib${lib}w.so "$BPM_OUTPUT"/usr/lib/lib${lib}.so
    ln -sfv ${lib}w.pc    "$BPM_OUTPUT"/usr/lib/pkgconfig/${lib}.pc
  done
  ln -sfv libncursesw.so "$BPM_OUTPUT"/usr/lib/libcurses.so
  mkdir -p "$BPM_OUTPUT"/usr/share/doc/ncurses/
  cp -v -R doc -T "$BPM_OUTPUT"/usr/share/doc/ncurses
  mkdir -p "$BPM_OUTPUT"/usr/share/licenses/ncurses
  cp COPYING "$BPM_OUTPUT"/usr/share/licenses/ncurses/
}
